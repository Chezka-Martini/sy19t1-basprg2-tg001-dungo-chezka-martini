#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void printCards(vector<string> cards) //print the cards for either side
{
	for (int i = 0; i < 5; i++)
	{
		cout << cards[i] << endl;
	}
}

int aiPlay(vector<string> cards)
{
	int aiRandomCard = rand() % 5;

	cout << cards[aiRandomCard] << endl;

	return aiRandomCard;
}

void playerCardRound(vector<string>* slave, vector<string>* emperor, string& userChoice, int round)
{
	for (int i = 0; i < round; i++)
	{
		if (i < 3)
		{
			cout << "You play the Emperor Side" << endl;
			cout << "Ai plays the Slave Side" << endl;
			cout << endl;

			system("pause");
			system("cls");

			printCards(*emperor);

			break;
		}
		else if (i < 6)
		{
			cout << "You play the Slave Side" << endl;
			cout << "Ai plays the Emperor Side" << endl;
			cout << endl;

			system("pause");
			system("cls");


			printCards(*slave);

			break;
		}
		else if (i < 9)
		{
			cout << "You play the Emperor Side" << endl;
			cout << "Ai plays the Emperor Side" << endl;
			cout << endl;

			system("pause");
			system("cls");

			printCards(*emperor);


			cout << endl;
			break;
		}
		else
		{
			cout << "You play the Slave Side" << endl;
			cout << "Ai plays the Emperor Side" << endl;
			cout << endl;

			system("pause");
			system("cls");

			printCards(*slave);

			cout << endl;
			break;
		}
	}
}
string aiCardRound(vector<string>* slave, vector<string>* emperor, string* aiCard, int round)
{
	for (int i = 0; i < round; i++)
	{
		if (i < 3)
		{
			return *aiCard = aiPlay(*slave);
			break;
		}
		else if (i < 6)
		{
			return *aiCard = aiPlay(*emperor);
			break;
		}
		else if (i < 9)
		{
			return *aiCard = aiPlay(*slave);
			break;
		}
		else
		{
			return *aiCard = aiPlay(*emperor);
			break;
		}
	}
}

int slaveCardMatchUp(int ear, int& bet, int& gold, string aiCard, string userChoice)
{

	if (aiCard == "\x1" || aiCard == "\x2" || aiCard == "\x3" || aiCard == "\x4")
	{
		if (userChoice == "Slave")
		{
			cout << "Ai Wins" << endl;
			ear -= bet;
			return gold = 0;
		}
		else if (userChoice == "Civilian")
		{
			cout << "DRAW" << endl;
			return gold = 0;
		}
	}
	else if (userChoice == "Slave" && aiCard == "")
	{
		cout << "Player Wins" << endl;
		return gold += (bet * 500000);
	}
}

int emperorCardMatchUp(int ear, int& bet, int& gold, string aiCard, string userChoice)
{

	if (aiCard == "\x1" || aiCard == "\x2" || aiCard == "\x3" || aiCard == "\x4")
	{
		if (userChoice == "Emperor")
		{
			cout << "Player Wins" << endl;
			return gold += (bet * 100000);
		}
		else if (userChoice == "Civilian")
		{
			cout << "DRAW" << endl;
			return gold = 0;
		}

	}
	else if (userChoice == "Emperor" && aiCard == "")
	{
		cout << "Ai Wins" << endl;
		ear -= bet;
		return gold = 0;
	}

}


void deleteCards(string userChoice, string aiCard, vector<string> slave, vector<string> emperor)
{
	if (userChoice == "Slave" || userChoice == "Emperor" || aiCard == "")
	{
		emperor.clear();
		slave.clear();
	}
	else if (userChoice == "Civilian" || aiCard == "\x1" || aiCard == "\x2" || aiCard == "\x3" || aiCard == "\x4")
	{
		emperor.pop_back();
		slave.pop_back();
	}
}

int main()
{
	srand(time(NULL));
	int round = 12;
	int bet = 0;
	int ear = 30;
	int gold = 0;
	string userChoice;
	string aiCard;
	string slaveCivilianCount[4];
	string emperorCivilianCount[4];

	vector<string> slave;
	slave.push_back("Slave");
	for (int i = 0; i < 4; i++)
	{
		slave.push_back("Civilian");
		slaveCivilianCount[i] = slave[i];
	}

	vector<string> emperor;
	emperor.push_back("Emperor");
	for (int i = 0; i < 4; i++)
	{
		emperor.push_back("Civilian");
		emperorCivilianCount[i] = emperor[i];
	}

	for (int i = 0; i < round; i++)
	{

		while (gold != 20000000 && ear != 0)
		{

			cout << "ROUND " << i + 1 << endl;

			cout << "Enter Milimeter Bet.  Your bet can only be valid within the remaining milimeter you have." << endl;
			cout << "You have " << ear << " milimeter to spare." << endl;
			cout << "What would your bet be: ";
			cin >> bet;
			cout << "Milimeter bet: " << bet << endl;

			while (bet > ear)
			{
				cout << "Re-Enter Milimeter Bet.  Your bet can only be valid within the remaining milimeter you have." << endl;
				cout << "You have " << ear << " milimeter to spare." << endl;
				cout << "What would your bet be: ";
				cin >> bet;

				cout << "Milimeter bet: " << bet << endl;
				break;
			}

			playerCardRound(&slave, &emperor, userChoice, round);

			cout << endl;

			cout << "What card would you like to play?: ";
			cin >> userChoice;

			cout << "Ai card: ";

			aiCard = aiCardRound(&slave, &emperor, &aiCard, round);

			if (i < 3)
			{
				 gold = emperorCardMatchUp(ear, bet, gold, aiCard, userChoice);
				
			}
			else if (i < 6)
			{
				gold = slaveCardMatchUp(ear, bet, gold, aiCard, userChoice);
			}
			else if (i < 9)
			{
				gold = emperorCardMatchUp(ear, bet, gold, aiCard, userChoice);
			
			}
			else
			{
				gold = slaveCardMatchUp(ear, bet, gold, aiCard, userChoice);
			
			}

			//deleteCards(userChoice, aiCard, slave, emperor);

			cout << endl;

			cout << "Your overall gold is " << gold << endl;
		}
	}

	if (gold == 20000000 && ear != 0)
	{
		cout << "Conratulations! You've reached your goal of 20,000,000.  Thank you for playing." << endl;
	}
	else if (gold > 0 && ear != 0)
	{
		cout << "You were not able to reach the goal of 20,000,000, however your ear is still safe.  Thank you for playing." << endl;
	}
	else if (ear == 0)
	{
		cout << "You were not able to save your ear and you can not wager or bet anymore.  Thank you for playing." << endl;
		cout << endl;
	}

	return 0;
	system("pause");
}



