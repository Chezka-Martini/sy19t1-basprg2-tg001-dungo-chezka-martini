#pragma once
#include<string>
#include<iostream>
#include"Items.h"

using namespace std;

class Character;

class Crystal 
	: public Items
{
public:
	Crystal(string name, int crystal);
	~Crystal();

	int getAddCrystal();

	void itemEffect(Character* character) override;

private:

	int aCrystal;
};

