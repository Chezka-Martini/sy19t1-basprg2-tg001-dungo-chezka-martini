#include "Character.h"

Character::Character(string name, int hp, int crystal, int points)
{
	cName = name;
	cHp = hp;
	cCrystal = crystal;
	cPoints = points;
}

Character::~Character()
{
}

string Character::getName()
{
	return cName;
}

int Character::getHp()
{
	return cHp;
}

int Character::getCrystal()
{
	return cCrystal;
}

int Character::getPoints()
{
	return cPoints;
}

void Character::takeDmg(int values)
{
	cHp -= values;
}

void Character::subtractCrystal(int values)
{
	cCrystal -= values;
}

void Character::addCrystal(int values)
{
	cCrystal += values;
}

void Character::heal(int values)
{
	cHp += values;
}

void Character::addPoint(int values)
{
	cPoints += values;
}
