#include "Bomb.h"
#include"Character.h"

Bomb::Bomb(string name, int dmg)
	:Items(name)
{
	cDmg = dmg;
}

Bomb::~Bomb()
{

}

int Bomb::getDmg()
{
	return cDmg;
}

void Bomb::itemEffect(Character* character)
{
	character->takeDmg(cDmg);

	cout << character->getName() << " took damage and now has an hp of " << character->getHp() << endl;
}
