#include<iostream>
#include<string>
#include<time.h>
#include<vector>
#include"Character.h"
#include"Bomb.h"
#include"HealthPotion.h"
#include"Crystal.h"
#include"RarityItems.h"

using namespace std;

struct Collate
{
	int healthPotion = 0;
	int bomb = 0;
	int crystal = 0;
	int SSR = 0;
	int SR = 0;
	int R = 0;
};
void PlayerStats(Character* character);
void AddItems(vector <Items*>& items); // adding all items in a vector
void ListItems(vector<Items*>& items); // listing ll the items
Items* getRandomItem(vector<Items*>& items, Character* character, Collate& item);// randomizing and getting the item(name and effect)
void GameSummary(Character* character, vector<Items*> loot);
void CollateItems(Collate& items);
void DeleteItems(Character* character, vector<Items*> items, vector<Items*> loot);
int main()
{
	srand(time(NULL));
	vector <Items*> items;
	string name;
	cout << "Enter the name of the character: ";
	cin >> name;
	Character* character = new Character(name, 100, 100, 0);

	PlayerStats(character);
	AddItems(items);
	ListItems(items);
	cout << "Your goal is to get 100 Rarity Points. Howeve, if your Hp or Crystal reaches 0 you will lose the game." << endl;
	vector <Items*> loot;
	Collate count;
	system("pause");
	system("cls");

	while (true)
	{
		cout << "The item you recieved is: ";
		loot.push_back(getRandomItem(items, character, count));
		character->subtractCrystal(5);

		if (character->getHp() <= 0 || character->getCrystal() <= 0)
		{
			cout << endl;
			cout << "You lose" << endl;
			system("pause");
			system("cls");
			break;
		}
		else if (character->getPoints() >= 100)
		{
			cout << endl;
			cout << "Congratulations!!" << endl;
			system("pause");
			system("cls");
			break;
		}

		system("pause");
	}
	GameSummary(character, loot);
	CollateItems(count);
	DeleteItems(character, items, loot);
	return 0;
}

void PlayerStats(Character* character)
{
	cout << "Character Name: " << character->getName() << endl;
	cout << "Health: " << character->getHp() << endl;
	cout << "Crystals: " << character->getCrystal() << endl;
	cout << "Rarity Points: " << character->getPoints() << endl;
}

void AddItems(vector <Items*>& items)
{
	HealthPotion* potion = new HealthPotion("Health Potion", 30);
	items.push_back(potion);

	Bomb* bomb = new Bomb("Bomb", 25);
	items.push_back(bomb);

	Crystal* crystals = new Crystal("Crystals", 15);
	items.push_back(crystals);

	RarityItems* ssr = new RarityItems("SSR", 50);
	items.push_back(ssr);

	RarityItems* sr = new RarityItems("SR", 10);
	items.push_back(sr);

	RarityItems* r = new RarityItems("R", 1);
	items.push_back(r);
}

void ListItems(vector<Items*>& items)
{
	cout << "Here are the things you might get while you pull: " << endl;
	for (int i = 0; i < items.size(); i++)
	{
		Items* item = items[i];

		cout << "Item " << i+1 << ": "<< item->getName() << endl;
	}
}

Items* getRandomItem(vector<Items*>& items, Character* character, Collate& item)
{
	float rng = (rand() % 100);
	Items* itemToGet = new Items("");
	rng /= 100;
	if (rng >= .40)
	{
		itemToGet = items.at(5);
		cout << itemToGet->getName() << endl;
		itemToGet->itemEffect(character);
		item.R++;
	}
	else if (rng >= .20)
	{
		itemToGet = items.at(1);
		cout << itemToGet->getName() << endl;
		itemToGet->itemEffect(character);
		item.bomb++;
	}
	else if (rng >= .15)
	{
		int random = rand() % 2;

		if (random == 0)
		{
			itemToGet = items.at(0);
			item.healthPotion++;
		}
		else
		{
			itemToGet = items.at(2);
			item.crystal++;
		}
		cout << itemToGet->getName() << endl;
		itemToGet->itemEffect(character);
	}
	else if (rng >= .09)
	{
		itemToGet = items.at(4);
		cout << itemToGet->getName() << endl;
		itemToGet->itemEffect(character);
		item.SR++;
	}
	else if (rng >= .01)
	{
		itemToGet = items.at(3);
		cout << itemToGet->getName() << endl;
		itemToGet->itemEffect(character);
		item.SSR++;
	}

	return itemToGet;
}

void GameSummary(Character* character, vector<Items*> loot)
{
	cout << "Here are the items you pulled:" << endl;
	for (int i = 0; i < loot.size(); i++)
	{
		Items* item = loot[i];

		cout << item->getName() << " | ";
	}
	cout << endl << endl;;
}

void CollateItems(Collate& items)
{
	cout << "Summary of Items you get: " << endl;
	cout << "Health Potion " << items.healthPotion << "X." << endl;
	cout << "Bombs " << items.bomb << "X." << endl;
	cout << "Crystals " << items.crystal << "X." << endl;
	cout << "R " << items.R << "X." << endl;
	cout << "SR " << items.SR << "X." << endl;
	cout << "SSR " << items.SSR << "X." << endl;
}

void DeleteItems(Character* character, vector<Items*> items, vector<Items*> loot)
{
	delete character; character = 0;

	items.clear();

	loot.clear();
}
