#pragma once
#include"Items.h"
#include<string>
#include<iostream>

using namespace std;

class Character;

class Bomb
	: public Items
{
public:
	Bomb(string name, int dmg);
	~Bomb();

	int getDmg();

	void itemEffect(Character* character) override;
private:
	int cDmg;
};

