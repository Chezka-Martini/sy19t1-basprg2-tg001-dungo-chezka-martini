#include "HealthPotion.h"
#include "Character.h"

HealthPotion::HealthPotion(string name, int heal)
	:Items(name)
{
	cHeal = heal;
}

HealthPotion::~HealthPotion()
{
}

int HealthPotion::getHeal()
{
	return cHeal;
}

void HealthPotion::itemEffect(Character* character)
{
	if (character->getHp() >= 100) return;

	character->heal(cHeal);

	cout << character->getName() << " healed for " << cHeal << ". and now has an of " << character->getHp()<< endl;
}
