#pragma once
#include"Items.h"
#include<string>
#include<iostream>

using namespace std;

class Character;

class HealthPotion 
	: public Items
{
public:
	HealthPotion(string name, int heal);
	~HealthPotion();

	int getHeal();
	void itemEffect(Character* character) override;

private:
	int cHeal;
};

