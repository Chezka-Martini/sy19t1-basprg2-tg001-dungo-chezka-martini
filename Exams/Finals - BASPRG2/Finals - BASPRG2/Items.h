#pragma once
#include<string>

using namespace std;

class Character;

class Items
{
public:
	  Items(string name);
	  ~Items();

	  string getName();
	  virtual void itemEffect(Character* character);

private:
	string iName;

};

