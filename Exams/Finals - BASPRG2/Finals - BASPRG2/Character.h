#pragma once
#include<string>

using namespace std;
class Character
{
public:
	Character(string name, int hp, int crystal, int points);
	~Character();

	string getName();
	int getHp();
	int getCrystal();
	int getPoints();

	void takeDmg(int values);
	void subtractCrystal(int values);
	void addCrystal(int values);
	void heal(int values);
	void addPoint(int values);
	
private:
	string cName;
	int cHp;
	int cCrystal;
	int cPoints;
};

