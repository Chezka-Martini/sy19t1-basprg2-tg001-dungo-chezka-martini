#pragma once
#include<string>
#include"Items.h"
#include <iostream>
using namespace std;

class Character;

class RarityItems
	: public Items
{
public:
	RarityItems(string name, int points);
	~RarityItems();

	int getAddPoints();

	void itemEffect(Character* character) override;
private:
	int itemPoints;
};

