#include "Crystal.h"
#include"Character.h"

Crystal::Crystal(string name, int crystal)
	:Items(name)
{
	aCrystal = crystal;
}

Crystal::~Crystal()
{

}

int Crystal::getAddCrystal()
{
	return aCrystal;
}

void Crystal::itemEffect(Character* character)
{
	character->addCrystal(aCrystal);

	cout << character->getName() << " received additional "<< aCrystal << " crystals and now has " << character->getCrystal() << endl;
}
