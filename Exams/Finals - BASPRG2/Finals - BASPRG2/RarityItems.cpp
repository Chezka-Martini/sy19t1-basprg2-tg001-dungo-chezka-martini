#include "RarityItems.h"
#include"Character.h"

RarityItems::RarityItems(string name, int points)
	:Items(name)
{
	itemPoints = points;
}

RarityItems::~RarityItems()
{
}

int RarityItems::getAddPoints()
{
	return itemPoints;
}


void RarityItems::itemEffect(Character* character)
{
	character->addPoint(itemPoints);

	cout << character->getName() << " recieved (" << itemPoints << ") rarity point and now has " << character->getPoints() << " points."<< endl;
}
