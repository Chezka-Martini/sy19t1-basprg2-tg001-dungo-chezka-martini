#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void sortPackages(int packages[])
{
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 7 - i - 1; j++)
		{
			if (packages[j] > packages[j + 1])
			{
				swap(packages[j + 1], packages[j]);
			}
		}
	}
}
void buyPackage(int &startingGold, string &userInput, int packages[], int &itemPrice)
{

	for (int n = 0; n < 7; n++) // to show package list
	{
		if (itemPrice < 250000)
		{
			if (itemPrice <= packages[n] && startingGold > 0)
			{
				cout << "We have a special offer for you " << endl;
				cout << "package " << n + 1 << " costs: " << packages[n] << endl << endl;

				cout << "Would You Like to Buy this Package? (YES / NO)" << endl;

				cin >> userInput;

				if (userInput == "YES")
				{
					startingGold += packages[n];
					cout << "Package Bought. Your Gold is now " << startingGold << endl << endl;

					startingGold -= itemPrice;
					cout << "Item bought. Your Gold is now: " << startingGold << endl << endl;
					break;
				}
				else if (userInput == "NO")
				{
					cout << "Package Not Bought" << endl << endl;
					break;
				}
			}

		}
		else
		{
			cout << "The item price is too high.  We do not have any package enough to buy the item. ";
			break;
		}
	}
		

}
void continueBuying(string &userinput, bool &userchoice)
{
	if (userinput == "YES")
	{
		userchoice = true;
		system("cls");
	}
	else if (userinput == "NO")
	{
		cout << "Thank you for buying! Have a nice day" << endl;
		userchoice = false;
	}
}

int main()
{
	srand(time(NULL));
	string userInput;
	int startingGold = 250;
	int itemPrice = 0;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	bool userChoice = true;

	while (userChoice == true)
	{
		cout << "Your Gold: " << startingGold << endl;
		cout << "Please enter the price of the item you would like to buy." << endl;
		cout << "Item Price: ";
		cin >> itemPrice;
		cout << endl;

		sortPackages(packages);
		if (startingGold <= itemPrice)
		{
			cout << "Not enough gold to buy the item." << endl << endl;
			buyPackage(startingGold, userInput, packages, itemPrice);
		}
		else if (startingGold >= itemPrice)
		{
			startingGold -= itemPrice;
			cout << "Item bought. Your Gold is now: " << startingGold << endl;
		}
		else
		{
			cout << "Item not bought." << endl;
		}

		cout << "Would You Like to continue Buying?: (YES / NO)" << endl;
		cin >> userInput;
		continueBuying(userInput, userChoice);
	}

	system("pause");
	return 0;
}