#pragma once
#include<string>

using namespace std;
class Character;

class Battle
{
public:

	Battle(Character* player, Character* enemy);
	~Battle();

	Character* getTarget();
	Character* getPlayer();
	int getDmg();

	void setDmg(int dmg);
	void makeDamage(Character* player, Character* target);

private:
	Character* cPlayer;
	Character* cEnemy;
	int cDmg;
};

