#include "Character.h"


Character::Character(string name, int Hp, int Pow, int Vit, int Agi, int Dex)
{
	cName = name;
	cHp = Hp;
	cPow = Pow;
	cVit = Vit;
	cAgi = Agi;
	cDex = Dex;
}

Character::~Character()
{
}

int Character::getHp()
{
	return cHp;
}

int Character::getPow()
{
	return cPow;
}

int Character::getVit()
{
	return cVit;
}

int Character::getAgi()
{
	return cAgi;
}

int Character::getDex()
{
	return cDex;
}

int Character::getHit()
{
	return cHit;
}

string Character::getName()
{
	return string(cName);
}

string Character::getType()
{
	return string(cType);
}

void Character::missChance(Character* player, Character* target)
{
	int hit = ((player->getDex() / target->getAgi()) * 100);

	cHit = hit;
}

void Character::takeDmg(int dmg)
{
	cHp -= dmg;
}

void Character::setType(string type)
{
	if (type == "1")
	{
		type = "Warrior";
	}
	else if (type == "2")
	{
		type = "Mage";
	}
	else if (type == "3")
	{
		type = "Assassin";
	}

	cType = type;
}

void Character::hpBack(int hp)
{
	cHp += (hp * .30);
}

void Character::addHp(int hp)
{
	hp += 3;

	cHp = hp;
}

void Character::addPow(int Pow)
{
	Pow += 5;

	cPow = Pow;
}

void Character::addVit(int Vit)
{
	Vit += 3;

	cVit = Vit;
}

void Character::addAgi(int Agi)
{
	Agi += 3;

	cAgi = Agi;
}

void Character::addDex(int Dex)
{
	Dex += 3;

	cDex = Dex;
}

