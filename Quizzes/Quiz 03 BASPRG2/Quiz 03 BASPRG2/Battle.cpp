#include "Battle.h"
#include"Character.h"

Battle::Battle(Character* player, Character* enemy)
{
	cPlayer = player;
	cEnemy = enemy;
}

Battle::~Battle()
{
}

Character* Battle::getTarget()
{
	return cEnemy;
}

Character* Battle::getPlayer()
{
	return cPlayer;
}

int Battle::getDmg()
{
	return cDmg;
}


void Battle::setDmg(int dmg)
{
	cDmg = dmg;
}

void Battle::makeDamage(Character* player, Character* target) //dmg with the bonus dmg as well
{
	int dmg = rand() % (player->getPow() - target->getVit());

	if (player->getType() == "Warrior")
	{
		if (target->getType() == "Assassin")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
		else if (target->getType() == "Mage")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
		else if (target->getType() == "Warrior")
		{
			dmg = rand() % (player->getPow() - target->getVit());
		}
	}
	else if (player->getType() == "Mage")
	{
		if (target->getType() == "Assassin")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
		else if (target->getType() == "Mage")
		{
			dmg = rand() % (player->getPow() - target->getVit());
		}
		else if (target->getType() == "Warrior")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
	}
	else if (player->getType() == "Assassin")
	{
		if (target->getType() == "Assassin")
		{
			dmg = rand() % (player->getPow() - target->getVit());
		}
		else if (target->getType() == "Mage")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
		else if (target->getType() == "Warrior")
		{
			dmg = (rand() % (player->getPow() - target->getVit()) * .50);
		}
	}

	if (dmg < 0)
	{
		dmg = 1;
	}

	if (target->getHit() < 20 && target->getHit() > 80)
	{
		dmg = 0;
	}

	cDmg = dmg;
	target->takeDmg(dmg);
}



