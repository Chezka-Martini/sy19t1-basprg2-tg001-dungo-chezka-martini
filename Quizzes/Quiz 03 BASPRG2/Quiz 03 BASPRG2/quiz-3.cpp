#include<iostream>
#include<string>
#include<time.h>
#include"Character.h"
#include"Battle.h"
using namespace std;


void baseStat(Character* avatar) // base stat for the player.
{
	cout << "Name: " << avatar->getName() << endl;
	cout << "Class: " << avatar->getType() << endl;
	cout << "Hp: " << avatar->getHp() << endl;
	cout << "Power: " << avatar->getPow() << endl;
	cout << "Vitality: " << avatar->getVit() << endl;
	cout << "Agility: " << avatar->getAgi() << endl;
	cout << "Dexterity: " << avatar->getDex() << endl;
}
void characterType(Character* player, string type)
{
	cout << "CLASSES" << endl;
	cout << "(1) [Warrior]" << endl << "(2) [Mage]" << endl << "(3) [Assassin]" << endl << endl;
	cout << "Enter what would be the class of your character: ";
	cin >> type;
	player->setType(type);

	cout << endl;
	baseStat(player);
	cout << endl;
	system("pause");
	system("cls");
}
void enemyCharacter(Character* enemy, string& type)
{
	int randomType = (rand() % 3) + 1;

	if (randomType == 1)
	{
		type = "Warrior";
	}
	else if (randomType == 2)
	{
		type = "Mage";
	}
	else
	{
		type = "Assassin";
	}
}
void enemySpawn(Character* player, Character* enemy, string type)
{
	cout << "Enemy Encounter" << endl;
	system("pause");

	cout << endl;
	baseStat(player);
	cout << endl;

	enemyCharacter(enemy, type);

	enemy->setType(type);

	baseStat(enemy);

	system("pause");
	system("cls");
}
void MissDmg(Battle* player)
{
	if (player->getDmg() == 0)
	{
		cout << "Attack missed!!" << endl;
	}
}
void addStats(Character* hero, Character* target)
{
	cout << "You gain this stats" << endl;
	if (target->getType() == "Warrior")
	{
		hero->addHp(hero->getHp());
		hero->addVit(hero->getVit());
		cout << "Hp: +3 " << endl;
		cout << "Vitality: +3" << endl;

	}
	else if (target->getType() == "Mage")
	{
		hero->addPow(hero->getPow());
		cout << "Power: +5" << endl;
	}
	else if (target->getType() == "Assassin")
	{
		hero->addAgi(hero->getAgi());
		hero->addDex(hero->getDex());
		cout << "Agility: +3" <<  endl;
		cout << "Dexterity: +3" << endl;
	}
}
void initializeBattle(Character* player, Character* enemy, Battle* round)
{
	if (player->getAgi() >= enemy->getAgi())
	{
		round->makeDamage(player, enemy);
		MissDmg(round);
		cout << "You dealt " << round->getDmg() << "  damage to enemy." << endl;
		cout << "Enemy Remaining HP: " << enemy->getHp() << endl << endl;

		round->makeDamage(enemy, player);
		MissDmg(round);
		cout << "Enemy dealt " << round->getDmg() << "  damage to you." << endl;
		cout << "Player Remaining HP: " << player->getHp() << endl << endl;
	}
	else if (player->getAgi() < enemy->getAgi())
	{
		round->makeDamage(enemy, player);
		MissDmg(round);
		cout << "Enemy dealt " << round->getDmg() << "  damage to you." << endl;
		cout << "Player Remaining HP: " << player->getHp() << endl << endl;

		round->makeDamage(player, enemy);
		MissDmg(round);
		cout << "You dealt " << round->getDmg() << "  damage to enemy." << endl;
		cout << "Enemy Remaining HP: " << enemy->getHp() << endl << endl;
	}
}

int main()
{
	srand(time(NULL));
	string name;
	string type;
	cout << "Enter the name of your character: ";
	cin >> name;

	Character* player = new Character(name, 30, (rand() % 10) + 1, (rand() % 10) + 1, (rand() % 10) + 1, (rand() % 10) + 1);// hp is set to 30 for longer enemy encounter.

	characterType(player, type);

	for (int i = 0; player->getHp() != 0; i++)
	{
		Character* enemy = new Character("Enemy", ((rand() % 50 - 10 + 1) + 10) + i, ((rand() % 10) + 1) + i, ((rand() % 10) + 1) + i, ((rand() % 10) + 1) + i, ((rand() % 10) + 1) + i);

		Battle* round = new Battle(player, enemy);

		if (player->getHp() <= 0) // to stop the loop if the player is dead
		{
			delete player; player = NULL;
			delete round; round = NULL;
			break;
		}

		enemySpawn(player, enemy, type);

		cout << "Initializing Battle" << endl;

		while (player->getHp() != 0 || enemy->getHp() != 0) //to initialize battle
		{
			initializeBattle(player, enemy, round);

			if (player->getHp() <= 0)
			{
				cout << "You dead" << endl;
				cout << "Stage reached: " << i + 1 << endl;
				break;
			}
			else if (enemy->getHp() <= 0)
			{
				cout << "Enemy dead" << endl << endl;

				addStats(player, enemy);
				player->hpBack(50);
				delete enemy; enemy = NULL;
				system("pause");
				system("cls");
				break;
			}
			system("pause");
		}
	}

	return 0;
}