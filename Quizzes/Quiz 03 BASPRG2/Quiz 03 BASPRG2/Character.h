#pragma once
#include<string>
using namespace std;

class Character
{
public:
	Character(string name, int Hp, int Pow, int Vit, int Agi, int Dex);
	~Character();

	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	int getHit();
	
	string getName();
	string getType();

	void missChance(Character* player, Character* target);
	void takeDmg(int dmg);
	void setType(string type);

	//If the player wins
	void hpBack(int hp);

	//Bonus stats to add
	void addHp(int Hp);
	void addPow(int Pow);
	void addVit(int Vit);
	void addAgi(int Agi);
	void addDex(int Dex);

private:
	int cHp;
	int cPow;
	int cVit;
	int cAgi;
	int cDex;
	string cName;
	string cType;
	int cDmg;
	int target;
	int cHit;
};

