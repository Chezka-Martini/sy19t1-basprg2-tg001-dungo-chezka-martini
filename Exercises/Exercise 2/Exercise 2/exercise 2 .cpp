#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int placeBet(int playerBet, int playerGold) // Exercise 2-1 (checking if the bet is valid.)
{
	if (playerBet <= playerGold && playerBet != 0)
	{
		cout << "Bet Valid." << endl;
		return playerBet;
	}
	else
	{
		cout << "Invalid bet.  The value should be lower or equal to the amount of gold you have. ";
		return playerBet = 0;
	}

}

int deductingGold(int playerBet, int& playerGold) // Exercise 2-1 (returning the bet and the gold)
{
	if (playerBet <= playerGold)
	{
		return playerGold -= playerBet;
	}
	else
	{
		return playerBet = 0;
	}
}

void betting(int& playerBet, int& playerGold) // Exercise 2-1, but the no void type.
{
	if (playerBet <= playerGold)
	{
		playerGold -= playerBet;
	}
	else
	{
		playerBet = 0;
	}
}

int roll(int firstDice, int secondDice, int& diceSum) // exercise 2-2
{
	firstDice = (rand() % 6) + 1;
	secondDice = (rand() % 6) + 1;

	return diceSum = firstDice + secondDice;
}

int Airoll(int aiFirstDice, int aiSecondDice, int& aiDiceSum) // exercise 2-2
{
	aiFirstDice = (rand() % 6) + 1;
	aiSecondDice = (rand() % 6) + 1;

	return aiDiceSum = aiFirstDice + aiSecondDice;
}

int diceMatch(int& diceSum, int& aiDiceSum, int& playerGold, int& playerBet) // exercise 2-3
{
	if (diceSum == 2 && diceSum != aiDiceSum)
	{
		cout << "Snake Eyes!!" << endl << "Player's Current Gold: ";
		playerBet *= 3;
		return playerGold += playerBet;
	}
	else if (aiDiceSum == 2 && aiDiceSum != diceSum)
	{
		cout << "Snake Eyes!!" << endl << "Player's Current Gold: ";
		return playerGold;
	}
	else if (diceSum == aiDiceSum)
	{
		cout << "Draw" << endl << "Player's Current Gold: ";
		return playerGold += playerBet;
	}
	else if (diceSum > aiDiceSum)
	{
		cout << "Player Wins" << endl << "Player's Current Gold: ";
		return playerGold += playerBet;
	}
	else if (aiDiceSum > diceSum)
	{
		cout << "Ai Wins" << endl << "Player's Current Gold: ";
		return playerGold;
	}
}

int rounds(int& diceSum, int& aiDiceSum, int& playerGold, int& playerBet, int firstDice, int secondDice, int aiFirstDice, int aiSecondDice) // exercise 2-4
{
	cout << "Remaining Gold: " << deductingGold(playerBet, playerGold) << endl;

	system("pause");
	system("cls");

	cout << "Ai Roll Sum: " << Airoll(aiFirstDice, aiSecondDice, aiDiceSum) << endl;
	cout << "Player Roll Sum: " << roll(firstDice, secondDice, diceSum) << endl;

	cout << "Dice Match	" << endl;
	cout << diceMatch(diceSum, aiDiceSum, playerGold, playerBet) << endl;

	system("pause");
	system("cls");
	return playerGold;
}

int main()
{
	srand(time(NULL));

	int playerBet = 0;
	int playerGold = 1000;
	int diceSum;
	int aiDiceSum;
	int firstDice = (rand() % 6) + 1;
	int secondDice = (rand() % 6) + 1;
	int aiFirstDice = (rand() % 6) + 1;
	int aiSecondDice = (rand() % 6) + 1;

	while (playerGold > 0)
	{
		cout << "Player's current gold: " << playerGold << endl;
		cout << "The game will only end once you lose all gold. " << endl;

		cout << "Please enter the value of your bet. Remember that the bet must be within the range of your current gold and you cannot play without putting your bet." << endl;
		cout << "bet: ";
		cin >> playerBet;
		cout << "Player Bet: " << placeBet(playerBet, playerGold) << endl;

		while (playerBet > playerGold || playerBet == 0)
		{
			cout << "bet: ";
			cin >> playerBet;
			cout << "Player Bet: " << placeBet(playerBet, playerGold) << endl;
		}
		rounds(diceSum, aiDiceSum, playerGold, playerBet, firstDice, secondDice, aiFirstDice, aiSecondDice);
	}
	if (playerGold == 0)
	{
		cout << "THANK YOU FOR PLAYING!" << endl;
	}
	return 0;
}