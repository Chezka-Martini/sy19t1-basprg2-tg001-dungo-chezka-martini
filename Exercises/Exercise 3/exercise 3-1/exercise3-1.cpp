#include <iostream>
#include <string>
#include <time.h>

using namespace std;
void fillArray(int* numbers)
{
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = (rand() % 100);
	}
}

int main()
{
	srand(time(NULL));
	int numbers[10];

	fillArray(numbers);

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
	system("pause");
	return 0;
}