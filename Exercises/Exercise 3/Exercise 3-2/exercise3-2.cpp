#include <iostream>
#include <string>
#include <time.h>

using namespace std;
void fillArray(int* numbers)
{
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = (rand() % 100);
	}
}

int main()
{
	srand(time(NULL));
	int* numbers =  new int[10]; // Exercise 3-2

	fillArray(numbers);

	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << " ";
	}

	delete[] numbers;	numbers = 0; // Exercise 3-3
	cout << endl;
	system("pause");
	return 0;
}