#pragma once
#include<iostream>
#include<string>
#include "Spell.h"
using namespace std;

class Wizard
{
private:
	string name;
	int mana;
	int hp;

public:
	Wizard();
	~Wizard();

	string wizardName() { return name; };
	void setWizardName(string name) { this->name = name; };
	int wizardMana() { return mana; };
	void setMana(int mana) { this->mana = mana; };
	int wizardHp() { return hp; };
	void setHp(int hp) { this->hp = hp; };

	void setWizardStats(string, int, int);
	void WizardStats();
	void castingSpell();
};

