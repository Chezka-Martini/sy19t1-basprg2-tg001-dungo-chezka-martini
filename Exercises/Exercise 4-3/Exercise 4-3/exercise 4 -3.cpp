#include<iostream>
#include<string>
#include<time.h>
#include"Wizard.h"
#include"Spell.h"
using namespace std;

int main()
{
	Wizard Mage1;
	Mage1.setWizardName("Mage 1");
	Mage1.setMana(200);
	Mage1.setHp(1000);

	Mage1.WizardStats();
	cout << endl;
	Mage1.castingSpell();
	cout << endl;

	return 0;
}
