#pragma once
#include<iostream>
#include<string>
using namespace std;

class Spell
{
private:
	string name = "Spell";
	int mpCost = 0;
	int dmg = 0;

public:

	string spellName() { return name; };
	void setSpellName(string name) { this->name = name; };
	int spellCost() { return mpCost; };
	void setMpCost(int mpCost) { this->mpCost = mpCost; };
	int spellDamage() { return dmg; };
	void setSpellDamage(int dmg) { this->dmg = dmg; };
	void castSpell(string, int, int);

	void spellCast();

	Spell();
	~Spell();

};

