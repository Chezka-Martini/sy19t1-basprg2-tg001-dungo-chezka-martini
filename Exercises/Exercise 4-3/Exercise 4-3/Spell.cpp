#include "Spell.h"
#include<iostream>
#include<string>
using namespace std;

Spell::Spell()
{
	this->name = name;
	this->mpCost = mpCost;
	this->dmg = dmg;
}

Spell::~Spell()
{
	cout << "The spell ( "<< this->name << " ) has been successfully used." << endl;
}

void Spell::castSpell(string name, int mpCost, int dmg)
{
	this->name = name;
	this->mpCost = mpCost;
	this->dmg = dmg;
}

void Spell::spellCast()
{
	cout << "The spell ( " << this->name << " ) has a mana cost of " << this->mpCost << " and deals " << this->dmg << endl;
}