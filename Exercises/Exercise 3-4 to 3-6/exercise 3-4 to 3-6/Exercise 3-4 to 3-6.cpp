#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int lootItem()
{
	struct Item
	{
		string itemOne = "Mithril Ore";
		string itemTwo = "Sharp Talon";
		string itemThree = "Thick Leather";
		string itemFour = "Jellopy";
		string itemFive = "Cursed Stone";

		int firstItem = 100;
		int secondItem = 50;
		int thirdItem = 25;
		int fourthItem = 5;
		int fifthItem = 0;
	}loot;

	int getLoot = (rand() % 5);
	int itemLooted = 0;
	if (getLoot == 0)
	{
		cout << "You found: " << loot.itemOne << endl << "Item Value: " << loot.firstItem << endl;
		itemLooted += loot.firstItem;
	}
	else if (getLoot == 1)
	{
		cout << "You found: " << loot.itemTwo << endl << "Item Value: " << loot.secondItem << endl;
		itemLooted += loot.secondItem;
	}
	else if (getLoot == 2)
	{
		cout << "You found: " << loot.itemThree << endl << "Item Value: " << loot.thirdItem << endl;
		itemLooted += loot.thirdItem;
	}
	else if (getLoot == 3)
	{
		cout << "You found: " << loot.itemFour << endl << "Item Value: " << loot.fourthItem << endl;
		itemLooted += loot.fourthItem;
	}
	else if (getLoot == 4)
	{
		cout << "You found: " << loot.itemFive << endl << "Item Value: " << loot.fifthItem;
		cout << " You picked up the cursed stone and you died (with all your loots lost). " << endl;
		itemLooted = 0;
	}
	return itemLooted;
}

int enterDungeon(int itemLooted, int* gold, string choice, int& getLoot)
{
	bool enter = true;
	*gold -= 25;

	int storage = 0;
	int multiplier = 1;
	bool contExploring = true;

	for (int i = 0; contExploring != false; i++)
	{
		storage += lootItem() * multiplier++;

		if (getLoot == 4)
		{
			contExploring = false;
			return storage = 0;
		}

		cout << "Theoretical Gold: " << storage << endl;

		cout << "Would you like to continue exploring? (YES) || (NO): ";
		cin >> choice;

		if (choice == "NO" || choice == "No" || choice == "no")
		{
			contExploring = false;
			return storage;
		}
		else if (choice == "YES" || choice == "Yes" || choice == "yes")
		{
			contExploring = true;
		}

	}
}

int main()
{
	srand(time(NULL));
	int gold = 50;
	int itemLooted = 0;
	string choice;
	int getLoot = 0;
	cout << "Welcome!" <<  endl;
	cout << "You will enter a dungeon with the goal of earning 500 gold.  Entering a dungeon will cost you 25 gold." << endl;

	while (gold != 0)
	{

		cout << "You exited the dungeon and your gold is now at " << enterDungeon(itemLooted, &gold, choice, getLoot) + gold << endl;
		cout << "Would you like to enter another dungeon?: (YES) || (NO)" << endl;
		cout << "Player chooses: ";
		cin >> choice;

		if (choice == "YES" || choice == "Yes")
		{
			enterDungeon(itemLooted, &gold, choice, getLoot);
		}
		else if (choice == "NO" || choice == "No")
		{
			cout << "Thank you for playing!!" << endl;
			break;
		}

		if (gold >= 500)
		{
			cout << "Congratulations on reaching 500 gold.  Thank you for playing!" << endl;
			break;
		}
		else if (gold == 0)
		{
			cout << "Awe you lost. . . . .  better luck next time.  Thank you for playing still" << endl;
			break;
		}
	}




	system("pause");
	return 0;
}