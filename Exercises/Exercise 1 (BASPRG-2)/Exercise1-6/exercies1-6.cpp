#include<iostream>
#include<string>
#include<time.h>

using namespace std;

int printSet(int numberSet[], int userInput)
{
	srand(time(NULL));
	int fill;
	for (int i = 0; i < userInput; i++)
	{
		fill = (rand() % 100) + 1;
		numberSet[i] = fill;
		cout << i + 1 << ") " << numberSet[i] << endl;
	}
	return numberSet[100];
}

void sortingArray(int numberSet[], int userInput)
{
	for (int i = 0; i < userInput; i++)
	{
		for (int j = 0; j < userInput - i - 1; j++)
		{
			if (numberSet[j] < numberSet[j + 1])
			{
				swap(numberSet[j + 1], numberSet[j]);
			}
		}
	}
	for (int n = 0; n < userInput; n++)
	{
		cout << n + 1 << ") " << numberSet[n] << endl;
	}
}

int main()
{
	int userInput = 0;
	int numberSet[100];

	cout << "Enter how many numbers to arranged: ";
	cin >> userInput;
	printSet(numberSet, userInput);
	cout << "Sorting Numbers in descending order: " << endl;
	sortingArray(numberSet, userInput);
	system("pause");
	return 0;
}