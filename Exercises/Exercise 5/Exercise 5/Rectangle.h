#pragma once
#include<string>
#include"Shapes.h"

using namespace std;

class Rectangle : public Shapes
{
public:
	Rectangle(int length, int width);
	~Rectangle();

	int getLength();
	int getWidth();

	int getArea() override;

private:
	int sLenght;
	int sWidth;
};

