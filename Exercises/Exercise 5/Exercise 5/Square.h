#pragma once
#include"Shapes.h"
#include<string>

using namespace std;
class Square : public Shapes
{
public:
	Square(int length);
	~Square();

	int getLength();
	int getArea() override;

private:
	int sLength;
};

