#pragma once
#include<string>
#include"Shapes.h"

class Circle : public Shapes
{
public:
	Circle(int radius);
	~Circle();

	int getRadius();

	int getArea() override;

private:
	int cRadius;
};

