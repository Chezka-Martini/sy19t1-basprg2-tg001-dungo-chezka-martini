#include<iostream>
#include<string>
#include"Shapes.h"
#include"Square.h"
#include"Rectangle.h"
#include"Circle.h"
#include<vector>

using namespace std;

int main()
{
	Square* square = new Square(3);
	square->setName("Square");
	square->setSides(4);

	Rectangle* rectangle = new Rectangle(3, 2);
	rectangle->setName("Rectangle");
	rectangle->setSides(4);

	Circle* circle = new Circle(3);
	circle->setName("Circle");
	circle->setSides(0);

	vector<Shapes*> shapes;

	shapes.push_back(square);
	shapes.push_back(rectangle);
	shapes.push_back(circle);

	for (int i = 0; i < shapes.size(); i++)
	{
		Shapes* shape = shapes[i];

		cout << "Name: " << shape->getName() << endl;
		cout << "No. of sides: " << shape->getSides() << endl;
		cout << "Area: " << shape->getArea() << endl;
	}
	return 0;
}