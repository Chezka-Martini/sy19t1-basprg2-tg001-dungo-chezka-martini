#include "Unit.h"

Unit::Unit(int hp, int pow, int vit, int agi, int dex)
{
	uHp = hp;
	uPower = pow;
	uVitality = vit;
	uAgility = agi;
	uDxterity = dex;
}

Unit::~Unit()
{
}

int Unit::getHp()
{
	return uHp;
}

int Unit::getPow()
{
	return uPower;
}

int Unit::getVit()
{
	return uVitality;
}

int Unit::getAgi()
{
	return uAgility;
}

int Unit::getDex()
{
	return uDxterity;
}

void Unit::AddHp()
{
	uHp += 10;
}

void Unit::AddPow()
{
	uPower += 2;
}

void Unit::AddVit()
{
	uVitality += 2;
}

void Unit::AddAgi()
{
	uAgility += 2;
}

void Unit::AddDex()
{
	uDxterity += 2;
}
