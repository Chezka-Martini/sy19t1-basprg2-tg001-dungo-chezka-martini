#pragma once
class Unit
{
public:
	Unit(int hp, int pow, int vit, int agi, int dex);
	~Unit();

	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void AddHp();
	void AddPow();
	void AddVit();
	void AddAgi();
	void AddDex();
private:
	int uHp;
	int uPower;
	int uVitality;
	int uAgility;
	int uDxterity;
};

