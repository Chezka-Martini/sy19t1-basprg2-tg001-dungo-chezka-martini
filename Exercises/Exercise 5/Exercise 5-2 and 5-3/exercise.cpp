#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include"Unit.h"

using namespace std;

void buffList(vector <int>& buffs)
{
	cout << "BUFF LISTS" << endl;
	for (int i = 0; i < 5; i++)
	{
		buffs[i];

		if (buffs[i] == 0)
		{
			cout << "Heal" << endl;
		}
		else if (buffs[i] == 1)
		{
			cout << "Might" << endl;
		}
		else if (buffs[i] == 2)
		{
			cout << "Iron Skin" << endl;
		}
		else if (buffs[i] == 3)
		{
			cout << "Concentration" << endl;
		}
		else if (buffs[i] == 4)
		{
			cout << "Haste" << endl;
		}
	}
}
void getBuff(vector <int>& buffs, Unit* unit)
{
	int r = rand() % 5;
	int count = 0;
	while (r != buffs[count])
	{
		count++;
		if (r == buffs[count])
		{
			break;
		}
	}
	if (buffs[count] == 0)
	{
		cout << " Heal" << endl;
		unit->AddHp();
	}
	else if (buffs[count] == 1)
	{
		cout << " Might" << endl;
		unit->AddPow();
	}
	else if (buffs[count] == 2)
	{
		cout << " Iron Skin" << endl;
		unit->AddVit();
	}
	else if (buffs[count] == 3)
	{
		cout << " Concentration" << endl;
		unit->AddDex();
	}
	else if (buffs[count] == 4)
	{
		cout << " Haste" << endl;
		unit->AddAgi();
	}
}
void displayStats(Unit* unit)
{
	cout << "HP: " << unit->getHp() << endl;
	cout << "POW: " << unit->getPow() << endl;
	cout << "VIT: " << unit->getVit() << endl;
	cout << "DEX: " << unit->getDex() << endl;
	cout << "AGI: " << unit->getAgi() << endl;
}
void exitProgram(string userChoice, bool& exit)
{
	cout << "Would you like to exit the program?: (Y) YES / (N) NO" << endl;
	cin >> userChoice;

	if (userChoice == "YES" || userChoice == "Y")
	{
		cout << "GoodBye." << endl;
		exit = true;
	}
	else if (userChoice == "NO" || userChoice == "N")
	{
		exit = false;
	}
}
int main()
{
	srand(time(NULL));
	bool exit = false;
	Unit* unit = new Unit(rand() % 50 + 1, rand() % 20 + 1, rand() % 20 + 1, rand() % 15 + 1, rand() % 15 + 1);
	vector <int> buffs;
	for (int i = 0; i < 5; i++)
	{
		buffs.push_back(i);
	}
	string userChoice;

	cout << "Your Character Stats are: " << endl;
	displayStats(unit);
	system("pause");

	while (exit != true)
	{
		buffList(buffs);
		cout << "The buff you got is";
		getBuff(buffs, unit);
		system("pause");
		system("cls");

		cout << "Your Character Stats are now: " << endl;
		displayStats(unit);
		system("pause");

		exitProgram(userChoice, exit);
	}


	return 0;
}